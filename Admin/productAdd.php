<?php 
include "../inc_connection.php";
include "inc_header.php";
	
	$varTampil = mysqli_query($connection, "SELECT * FROM category ORDER BY category_id");
?>

		<!--Start Content-->
		<div id="content" class="col-xs-12 col-sm-10">
		<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<a href="#" class="show-sidebar">
			<i class="fa fa-bars"></i>
		</a>
		<ol class="breadcrumb pull-left">
			<li><a href="adminArea.php">Dashboard</a></li>
			<li><a href="product.php">Product</a></li>
			<li><a href="productAdd.php">Product Add</a></li>
		</ol>
		<div id="social" class="pull-right">
			<a href="#"><i class="fa fa-google-plus"></i></a>
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-youtube"></i></a>
		</div>
	</div>
</div>
<div class="box-name">
	<form action="productAdd_Save.php" method="post">
				<div class="box">
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Add Product</h4>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-2 control-label">Product Code</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Code" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="code">
						</div>
						<label class="col-sm-2 control-label">Category ID</label>
						<div class="col-sm-4">
							<select class="form-control" placeholder="Stok" name="categoryid">
								<option>--Choose Category--</option>
								<?php 
									while ($varData = mysqli_fetch_array($varTampil)) {
										echo "<option value=$varData[category_id]>$varData[category_name]</option>";
									}
									 ?>
							</select>
						</div>
						<label class="col-sm-2 control-label">Product Name</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Name" data-toggle="tooltip" data-placement="bottom" title="Tooltip for last name" name="name">
						</div>
					</div>
					<br>
					<br>
					<div class="form-group has-success has-feedback">
						<label class="col-sm-2 control-label">Product Price Sale</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="Price Sale" name="sale">
						</div>
						<label class="col-sm-2 control-label">Stok</label>
						<div class="col-sm-4">
							<select class="form-control" placeholder="Stok" name="number">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="form-styles">Product Description</label>
						<div class="col-sm-10">
								<textarea class="form-control" rows="5" id="wysiwig_simple" name="desc"></textarea>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-2">
							<button type="cancel" class="btn btn-default btn-label-left" name="cancel">
							<span><i class="fa fa-clock-o txt-danger"></i></span>
								Cancel
							</button>
						</div>
						<div class="col-sm-2">
							<input type="submit" class="btn btn-primary btn-label-left" name="save">
							<span><i class="fa fa-clock-o"></i></span>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-warning btn-label-left" name="back">
							<span><i class="fa fa-clock-o"></i></span>
								<a href="product.php">Back</a>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</form>
</div>

<?php include "inc_footer.php"; ?>