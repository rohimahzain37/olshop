<?php 
include "../inc_connection.php";
include "inc_header.php";
	
	$varCatID = $_GET['category_id'];
	$varQuery = mysqli_query($connection, "SELECT category_id,category_name FROM category WHERE category_id = '$varCatID' ");
	$varData  = mysqli_fetch_assoc($varQuery);

 ?>

		<!--Start Content-->
		<div id="content" class="col-xs-12 col-sm-10">
			<div class="row">
				<div id="breadcrumb" class="col-xs-12">
					<a href="#" class="show-sidebar">
						<i class="fa fa-bars"></i>
					</a>
		<ol class="breadcrumb pull-left">
			<li><a href="adminArea.php">Dashboard</a></li>
			<li><a href="category.php">Category</a></li>
			<li><a href="categoryAdd.php">Category Update</a></li>
		</ol>
		<div id="social" class="pull-right">
			<a href="#"><i class="fa fa-google-plus"></i></a>
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-youtube"></i></a>
		</div>
	</div>
</div>


	<form action="categoryUpdate_Save.php" method="post">
		<div class="container-fluid">
			<div id="page-login" class="row">
				<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<div class="box">
						<div class="box-content">
							<div class="text-center">
								<h3 class="page-header">Update Category Name</h3>
							</div>
							<div class="form-group">
								<input type="hidden" name="catname" value="<?php echo $varData['category_id']?>">
								<label class="control-label">Category Name</label>
								<input type="text" class="form-control" name="categoryname" required="true" id="category_name" value="<?php echo $varData['category_name']?>" />
							</div>
							<div class="text-center">
								<input type="submit" class="btn btn-primary" name="savecategory"></input>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>