<?php 
include "../inc_connection.php";
include "inc_header.php";

if ($_GET['product_code']) {
			$varQuery = mysqli_query($connection, "SELECT * FROM product LEFT JOIN category ON product.category_id = category.category_id WHERE product_code = '$_GET[product_code]' ");
			$varData  =mysqli_fetch_assoc($varQuery);
		}

//UPLOAD FOTO
$target_dir = "img/";
$flag[] = true;

$sql = "SELECT p.product_code, count(i.product_code) AS image_limit FROM product p JOIN image i ON i.product_code = p.product_code WHERE p.product_code = '$_GET[product_code]'";
$query = mysqli_query($connection, $sql);
$get_old_data = mysqli_fetch_row($query);

	if(isset($_POST['submit'])) {
		if ($get_old_data[1] >= 3) {
	echo"<script>alert('Maaf!!! Product memiliki 3 gambar');
						window.location=('product.php')</script>";
} else {
		foreach ($_FILES["image_products"]["name"] as $key => $value) {
			$varProductCode = $_POST['productcode'];
			$img_name	= $_FILES["image_products"]["name"][$key];
			$img_tmpname = $_FILES["image_products"]["tmp_name"][$key];
			$img_size = $_FILES = $_FILES["image_products"]["size"][$key];
			$img_ext = strtolower(pathinfo($target_dir.$img_name,PATHINFO_EXTENSION));
			$img_check = getimagesize($img_tmpname);
			if($img_check !== false) {
				$flag = true;
			} else {
				echo "Bukan Gambar";
				$flag = true;
			}
			if($img_size > 2097152) {
				echo "Maksimum Gambar 2 MB";
				$flag = false;
			}

			if($img_ext != "jpg" && $img_ext !="png" && $img_ext !="jpeg") {
				echo"Hanya menerima JPG/PNG";
				$flag = false;
			}
			$new_name = $key .'_'.rand(1,4).'_'.$img_name ;

			if($flag == false) {
				$message['error']['save'] = 'Gambar gagal diunggah.';

			} else {
				if(move_uploaded_file($img_tmpname, $target_dir.$new_name)) {
					//insert ke database
					$datetimenow = date('Y-m-d H:i:s');
					$varSQL = "INSERT INTO image(image_name,image_date,product_code) VALUES ('{$new_name}', '{$datetimenow}', '$varProductCode')";
					$query = mysqli_query($connection, $varSQL);
					$message['success']['submit'] = "Gambar Berhasil Diunggah";
				} else {
					$message['error']['submit'] = "Gambar Gagal Diunggah";
				}
			}
		}
	}
	}


//==================================================	
				
?>

		<!--Start Content-->
		<div id="content" class="col-xs-12 col-sm-10">
		<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<a href="#" class="show-sidebar">
			<i class="fa fa-bars"></i>
		</a>
		<ol class="breadcrumb pull-left">
			<li><a href="adminArea.php">Dashboard</a></li>
			<li><a href="product.php">Product</a></li>
			<li><a href="productAdd.php">Product Add</a></li>
		</ol>
		<div id="social" class="pull-right">
			<a href="#"><i class="fa fa-google-plus"></i></a>
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-youtube"></i></a>
		</div>
	</div>
</div>
<div class="box-name">
	<form action="" method="post" enctype="multipart/form-data">
				<div class="box">
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Add Product</h4>
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-4">
							<input type="hidden" class="form-control" placeholder="" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="productcode" value="<?php echo $varData['product_code']?>">
						</div>
						<br>
					<div class="form-group">
						<label class="col-sm-2 control-label">Product Name</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="productname" value="<?php echo $varData['product_name']?>">
						</div> 
						<div class="form-group">
						<label class="col-sm-2 control-label">Category Name</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" placeholder="" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="categoryname" value="<?php echo $varData['category_name']?>">
						</div>
						<label class="col-sm-2 control-label">Product Image</label>
						<div class="col-sm-4">
							<input type="file" class="form-control" placeholder="Name" data-toggle="tooltip" data-placement="bottom" title="Tooltip for last name" name="image_products[]" multiple>
						</div>
						<div class="col-sm-4">
							<input type="hidden" class="form-control" placeholder="Product Name" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name" name="product">
						</div>
					</div>
					<br>
					<br>
					<div class="clearfix"></div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-2">
							<button type="cancel" class="btn btn-default btn-label-left" name="cancel">
							<span><i class="fa fa-clock-o txt-danger"></i></span>
								Cancel
							</button>
						</div>
						<div class="col-sm-2">
							<input type="submit" class="btn btn-primary btn-label-left" name="submit" value="Unggah">
							<span><i class="fa fa-clock-o"></i></span>
						</div>
						<div class="col-sm-2">
							<button type="submit" class="btn btn-warning btn-label-left" name="back">
							<span><i class="fa fa-clock-o"></i></span>
								<a href="product.php">Back</a>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</form>
</div>
