<?php 
include "../inc_connection.php";
include "inc_header.php";

$Varsql     = "SELECT category_id, category_name FROM category";

      $cari = isset($_POST['cari'])? $_POST['cari'] : '';
      if (!empty($cari)) {
        $Varsql .= " WHERE category_name, category_id LIKE '%$cari%' ";
      } 
      $VarResult    = mysqli_query($connection,$Varsql);

 ?>

		<!--Start Content-->
		 <div id="content" class="col-xs-12 col-sm-10">
		<div class="row">
	<div id="breadcrumb" class="col-xs-12">
		<a href="#" class="show-sidebar">
			<i class="fa fa-bars"></i>
		</a>
		<ol class="breadcrumb pull-left">
			<li><a href="adminArea.php">Dashboard</a></li>
			<li><a href="category.php">Category</a></li>
		</ol>
		<div id="social" class="pull-right">
			<a href="#"><i class="fa fa-google-plus"></i></a>
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-youtube"></i></a>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa-plus-square"></i>
					<span><a href="categoryAdd.php">Add Category</a></span>
				</div>
				<div class="box-icons">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content no-padding">
				<table class="table table-bordered table-striped table-hover table-heading table-datatable" id="datatable-1">
					<thead>
						<tr>
							<th>NO</th>
							<th>CATEGORY ID</th>
							<th>CATEGORY NAME</th>
							<th>OPTION</th>
						</tr>
    <?php  

        $no       = 0;
        $no++;

        $varCheck = mysqli_num_rows($VarResult);
        if ($varCheck<1) {
          echo "<script>alert('Product Not Found');
          window.location=('category.php')</script>";
        }
        while ($varData = mysqli_fetch_array($VarResult)){
        //cek apakah nomernya ganjil atau genap
          echo" <td align='center'>".$no."</td>";
          echo" <td >".$varData['category_id']."</td>";
          echo" <td >".$varData['category_name']."</td>";
          echo" <td>"
    ?>
    	<a href="categoryUpdate.php?category_id=<?php echo $varData['category_id']; ?>">Update</a>
        <a href="categoryDelete.php?category_id=<?php echo $varData['category_id']; ?>">Delete</a>
        	
    <?php
      echo"</td>";
      echo"</tr>";
      $no++;
    ?>
    <?php }  ?>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>