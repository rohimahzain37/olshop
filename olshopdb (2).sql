-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Jan 2019 pada 17.05
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `olshopdb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `admin_id` tinyint(4) NOT NULL,
  `admin_name` varchar(20) NOT NULL,
  `admin_username` varchar(50) NOT NULL,
  `admin_password` char(32) NOT NULL,
  `admin_photo` varchar(20) NOT NULL,
  `level_id` tinyint(4) NOT NULL,
  `admin_login_status` enum('Y','N') NOT NULL DEFAULT 'N',
  `admin_status_blok` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_username`, `admin_password`, `admin_photo`, `level_id`, `admin_login_status`, `admin_status_blok`) VALUES
(18, 'Admin', 'admin123', '0192023a7bbd73250516f069df18b500', '35.jpg', 0, 'N', 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin_counter`
--

CREATE TABLE `admin_counter` (
  `admin_counter_id` int(11) NOT NULL,
  `admin_counter_number` tinyint(4) NOT NULL,
  `admin_counter_date` date NOT NULL,
  `admin_id` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin_counter`
--

INSERT INTO `admin_counter` (`admin_counter_id`, `admin_counter_number`, `admin_counter_date`, `admin_id`) VALUES
(6, 1, '2018-10-08', 9),
(7, 3, '2018-12-25', 13);

-- --------------------------------------------------------

--
-- Struktur dari tabel `booking`
--

CREATE TABLE `booking` (
  `booking_id` tinyint(4) NOT NULL,
  `id_detail_booking` tinyint(4) NOT NULL,
  `customer_id` tinyint(4) NOT NULL,
  `product_code` tinyint(4) NOT NULL,
  `date` date NOT NULL,
  `count` tinyint(3) NOT NULL,
  `total` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `booking`
--

INSERT INTO `booking` (`booking_id`, `id_detail_booking`, `customer_id`, `product_code`, `date`, `count`, `total`) VALUES
(47, 14, 9, 100, '2019-01-14', 3, 127);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE `cart` (
  `cart_id` tinyint(4) NOT NULL,
  `customer_id` tinyint(4) DEFAULT NULL,
  `product_code` char(3) NOT NULL,
  `date` date NOT NULL,
  `count` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cart`
--

INSERT INTO `cart` (`cart_id`, `customer_id`, `product_code`, `date`, `count`) VALUES
(86, 9, '100', '2019-01-14', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE `category` (
  `category_id` tinyint(4) NOT NULL,
  `category_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(10, 'Households'),
(11, 'Veggies'),
(12, 'Fruits'),
(13, 'Kitchen'),
(16, 'Mashroom');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `customer_id` tinyint(4) NOT NULL,
  `customer_name` varchar(20) NOT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_phone` varchar(20) NOT NULL,
  `customer_email` varchar(50) NOT NULL,
  `customer_password` char(32) NOT NULL,
  `customer_identity` char(16) NOT NULL,
  `customer_gender` enum('F','M') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `customer_address`, `customer_phone`, `customer_email`, `customer_password`, `customer_identity`, `customer_gender`) VALUES
(9, 'Rohimah', 'Wonosobo', '085601437195', 'Rohimahzain37@gmail.com', '7d8b0848ae654df885de7a2c0c615ceb', '3307059706020002', 'F');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_booking`
--

CREATE TABLE `detail_booking` (
  `id_detail_booking` tinyint(4) NOT NULL,
  `total_pembelian` varchar(50) NOT NULL,
  `customer_id` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_booking`
--

INSERT INTO `detail_booking` (`id_detail_booking`, `total_pembelian`, `customer_id`) VALUES
(14, '36000', 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transaction`
--

CREATE TABLE `detail_transaction` (
  `id_detail_transaction` tinyint(4) NOT NULL,
  `total_pembelian` varchar(50) NOT NULL,
  `customer_id` tinyint(4) NOT NULL,
  `id_detail_booking` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_transaction`
--

INSERT INTO `detail_transaction` (`id_detail_transaction`, `total_pembelian`, `customer_id`, `id_detail_booking`) VALUES
(5, '36000', 9, 14);

-- --------------------------------------------------------

--
-- Struktur dari tabel `image`
--

CREATE TABLE `image` (
  `image_id` smallint(6) NOT NULL,
  `image_name` varchar(20) NOT NULL,
  `image_date` datetime NOT NULL,
  `product_code` char(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `image`
--

INSERT INTO `image` (`image_id`, `image_name`, `image_date`, `product_code`) VALUES
(43, '0_4_29.png', '2018-12-14 07:30:00', '001'),
(44, '0_2_11.png', '2018-12-14 07:30:12', '002'),
(45, '0_4_10.png', '2018-12-14 07:30:27', '003'),
(46, '0_2_35.png', '2018-12-16 03:55:03', '005'),
(47, '0_4_36.png', '2018-12-16 03:57:15', '004'),
(48, '0_1_35.png', '2018-12-21 08:19:14', '100'),
(49, '0_1_12.png', '2019-01-12 16:58:34', '300'),
(50, '0_3_32.png', '2019-01-12 17:01:55', '400');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE `product` (
  `product_code` char(3) NOT NULL,
  `product_name` varchar(20) NOT NULL,
  `product_desc` mediumtext NOT NULL,
  `product_number` smallint(3) NOT NULL,
  `product_price_sale` int(10) DEFAULT NULL,
  `category_id` tinyint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`product_code`, `product_name`, `product_desc`, `product_number`, `product_price_sale`, `category_id`) VALUES
('001', 'Banana', 'Banana is Pisang', 4, 10000, 12),
('002', 'Apple', 'Apple is a tropical fruit', 1, 100000, 12),
('004', 'Strowberry', 'Strawwberry is tropical fruit and the taste is sour', 9, 30000, 12),
('100', 'Mashrom', 'Mashroom Is Jamur', 5, 12000, 16),
('300', ' Broccoli', '\r\nBroccoli is a green vegetable that contains lots of vitamins', 3, 20000, 11),
('400', 'Orange', 'Tropical Fruits', 7, 20000, 12);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction`
--

CREATE TABLE `transaction` (
  `id_transaction` int(11) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `mobile_phone` varchar(15) NOT NULL,
  `city` varchar(50) NOT NULL,
  `status` enum('Lunas','Belum Lunas') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indeks untuk tabel `admin_counter`
--
ALTER TABLE `admin_counter`
  ADD PRIMARY KEY (`admin_counter_id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indeks untuk tabel `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `product_code` (`product_code`);

--
-- Indeks untuk tabel `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `product_code` (`product_code`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indeks untuk tabel `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indeks untuk tabel `detail_booking`
--
ALTER TABLE `detail_booking`
  ADD PRIMARY KEY (`id_detail_booking`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indeks untuk tabel `detail_transaction`
--
ALTER TABLE `detail_transaction`
  ADD PRIMARY KEY (`id_detail_transaction`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `id_detail_booking` (`id_detail_booking`);

--
-- Indeks untuk tabel `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `product_code` (`product_code`);

--
-- Indeks untuk tabel `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_code`),
  ADD KEY `category_id` (`category_id`);

--
-- Indeks untuk tabel `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id_transaction`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `admin_counter`
--
ALTER TABLE `admin_counter`
  MODIFY `admin_counter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT untuk tabel `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT untuk tabel `category`
--
ALTER TABLE `category`
  MODIFY `category_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `detail_booking`
--
ALTER TABLE `detail_booking`
  MODIFY `id_detail_booking` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `detail_transaction`
--
ALTER TABLE `detail_transaction`
  MODIFY `id_detail_transaction` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `image`
--
ALTER TABLE `image`
  MODIFY `image_id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id_transaction` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
