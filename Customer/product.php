<?php 
	include "../inc_connection.php";
	session_start();

		$varCounter=0;
		$varSql = mysqli_query($connection, "SELECT * FROM product");
						while ($varData=mysqli_fetch_array($varSql)) {
								//$varCounter = $varCounter + 1;
								$varCode = $varData['product_code'];
								$varName = $varData['product_name'];
								$varDesc = $varData['product_desc'];
								$varPrice = $varData['product_price_sale'];
								$varAmount = $varData['product_number'];

	$sql = "SELECT p.product_code, p.product_name, p.product_desc, p.product_number, p.product_price_sale, c.category_id FROM product p JOIN category c ON c.category_id = p.category_id ORDER BY p.product_code DESC ";
	$query = mysqli_query($connection, $sql);
	$count = mysqli_num_rows($query);
	$varTampil = mysqli_query($connection, "SELECT * FROM product ORDER BY product_code");
	}
	
	if(isset($_POST['submit'])) {
		$_SESSION['productCode'] = $_POST['pilih'];
		$_SESSION['productQuantity'] = $_POST['productnumber'];
		$_SESSION['productQuantity2'] = array_filter($_SESSION['productQuantity']);

		$_SESSION['cart'] = array_combine($_SESSION['productCode'], $_SESSION['productQuantity2']);

	if(!isset($_SESSION['CustomerId'])) {
		header("location:login.php");
	} else {
		foreach ($_SESSION['cart'] as $key => $value) {
			$varInsert = mysqli_query($connection, "INSERT INTO cart(customer_id, date, product_code, count) VALUES ('".$_SESSION['CustomerId']."', NOW(), '".$key."', '".$value."')");						
			header("location:cart.php");
		}
	}
	}
 ?>

  <?php include "inc_header.php"; ?>

			<!-- flexSlider -->
				<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
				<script defer src="js/jquery.flexslider.js"></script>
				<script type="text/javascript">
				$(window).load(function(){
				  $('.flexslider').flexslider({
					animation: "slide",
					start: function(slider){
					  $('body').removeClass('loading');
					}
				  });
				});
			  </script>
			<!-- //flexSlider -->
		</div>
		<div class="clearfix"></div>
	</div>	
	<div class="top-brands">
		<div class="container">
			<h3>PRODUCTS</h3>
			<form action="" method="post">
			<div class="agile_top_brands_grids">
				<?php if($count > 0) :	 ?>
					<?php while($data = mysqli_fetch_array($query)) : ?>
						<?php $varCounter = $varCounter + 1;?>
				<div class="col-md-3 top_brand_left">
					<div class="hover14 column">
						<div class="agile_top_brand_left_grid">
							<?php 
								$sql_img = "SELECT image_name FROM image WHERE product_code = '{$data['product_code']}' LIMIT 1";
								$query_img = mysqli_query($connection, $sql_img);
								$count_img = mysqli_num_rows($query_img);
								$data_img  = mysqli_fetch_array($query_img);

							 ?>

							<div class="tag"><img src="images/tag.png" alt=" " class="img-responsive" /></div>
							<div class="agile_top_brand_left_grid1">
								<figure>
									<div class="snipcart-item block" >
										<div class="snipcart-thumb">
											<a href="desc.php?product_code=<?php echo $data['product_code']; ?>"><img title=" " alt=" " src="../Admin/img/<?php echo $data_img['image_name']?>" /></a>		
											<p><center><?php echo $data['product_name']?></center></p>
											<h4><center><?php echo $data['product_price_sale']?></center></h4>
											<h5 align="center"><input type="checkbox" name="pilih[]" value="<?php echo $data['product_code']; ?>">Pilih
											<select name="productnumber[]">
											<?php
												for ($i=0; $i <= $varAmount; $i++) { 
											?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
											<?php
												}
											?>											
										</select>
										
										<div class="snipcart-details top_brand_home_details">
											
										</div>
									</div>

								</figure>
							</div>
						</div>
					</div>
				</div>
				<?php endwhile ?>
			<?php endif ?>
				<div class="clearfix"> </div>
			</div>
			<br>
		</div>
		<div align="center">
		<input type="hidden" name="counter" value=<?php echo $varCounter; ?>>
		<input type="submit" name="submit" value="Add to cart" class="button" />
	</div>
	</form>
	</div>
<!-- //top-brands -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-3 w3_footer_grid">
				<h3>information</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="events.php">Events</a></li>
					<li><a href="about.php">About Us</a></li>
					<li><a href="index.php">Product</a></li>
					<li><a href="services.php">Services</a></li>
					<li><a href="short-codes.php">Short Codes</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>policy info</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="faqs.php">FAQ</a></li>
					<li><a href="privacy.php">privacy policy</a></li>
					<li><a href="privacy.php">terms of use</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>what in stores</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="pet.php">Pet Food</a></li>
					<li><a href="frozen.php">Frozen Snacks</a></li>
					<li><a href="kitchen.php">Kitchen</a></li>
					<li><a href="index.php">Branded Foods</a></li>
					<li><a href="household.php">Households</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>twitter posts</h3>
				<ul class="w3_footer_grid_list1">
					<li><label class="fa fa-twitter" aria-hidden="true"></label><i>01 day ago</i><span>Non numquam <a href="#">http://sd.ds/13jklf#</a>
						eius modi tempora incidunt ut labore et
						<a href="#">http://sd.ds/1389kjklf#</a>quo nulla.</span></li>
					<li><label class="fa fa-twitter" aria-hidden="true"></label><i>02 day ago</i><span>Con numquam <a href="#">http://fd.uf/56hfg#</a>
						eius modi tempora incidunt ut labore et
						<a href="#">http://fd.uf/56hfg#</a>quo nulla.</span></li>
				</ul>
			</div>
			
				<div class="clearfix"> </div>
			</div>
			<div class="wthree_footer_copy">
				<p>© 2018 Rohimah 3163111042 D3 Sistem Informasi</p>
			</div>
		</div>
	</div>

	 <?php include "inc_footer.php"; ?>