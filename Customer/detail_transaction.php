<?php 

include_once '../inc_connection.php';
	session_start();

	if (!isset($_SESSION['CustomerId'])) {
		header("location:login.php");
	} else {

	}

 ?>

  <?php include "inc_footer.php"; ?>


<!-- about -->
		<div class="privacy about">
			<h3>Chec<span>kout</span></h3>
			
	      <div class="checkout-right">
					<h4>Your shopping cart contains: </h4>
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>SL No.</th>	
							<th>Product</th>
							<th>Quality</th>
							<th>Product Name</th>
							<th>Price</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
							<?php
								$varQuery = mysqli_query($connection, "SELECT * FROM booking LEFT JOIN product ON product.product_code = booking.product_code WHERE id_detail_booking = '".$_GET['id']."'");
								$varCount = 0;
								$varTotal = 0;
								while ($varData = mysqli_fetch_array($varQuery)) {
									$varCount = $varCount+1;
									$varCode = $varData['product_code'];
									$varName = $varData['product_name'];
									$varQuantity = $varData['count'];
									$varPrice = $varData['product_price_sale'];
								$varSqlImg = mysqli_query($connection, "SELECT * FROM image WHERE product_code='$varCode' ORDER BY image_id DESC LIMIT 1");
								$varDataImg = mysqli_fetch_array($varSqlImg);
								$varImage = $varDataImg['image_name'];
							?>							
						<tr class="rem1">
						<td class="invert"><?php echo $varCount; ?></td>
						<td class="invert-image"><a href="single.html"><img src="../Admin/img/<?php echo $varImage; ?>" alt=" " class="img-responsive"></a></td>
						<td class="invert">
							 <div class="quantity"> 
								<div class="quantity-select">                           
									<div class="entry value-minus">&nbsp;</div>
									<div class="entry value"><span><?php echo $varQuantity; ?></span></div>
									<div class="entry value-plus active">&nbsp;</div>
								</div>
							</div>
						</td>
						<td class="invert"><?php echo $varName; ?></td>
						
						<td class="invert">Rp. <?php echo number_format($varPrice); ?></td>
						<td class="invert"><?php echo "Rp.".number_format($Subtotal=$varQuantity*$varPrice); ?></td>
					</tr>
				<?php 
				$varTotal = $varTotal+($varQuantity*$varPrice);
			} ?>
				<tr>
					<td colspan="4">Total</td>
					<td>
						<?php echo number_format($varTotal); ?>
					</td>
					<td></td>
				</tr>
				</tbody></table>
			</div>
			
									</section>
								</form>
						<br>			
				
									<div class="checkout-right-basket">
				        	<a href="paymentProses.php?id=<?php echo $_GET['id'] ?>">Make a Payment <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
			      	</div>
					</div>
			
					</div>
				<div class="clearfix"> </div>
				
			</div>

		</div>
<!-- //about -->
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->

 <?php include "inc_footer.php"; ?>