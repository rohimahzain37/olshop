<?php 

include_once '../inc_connection.php';
	session_start();

	if (!isset($_SESSION['CustomerId'])) {
		header("location:login.php");
	} else {

	}

 include "inc_header.php";
 ?>
	
<!-- about -->
		<div class="privacy about">
			
	      <div class="checkout-right">
					<h4>Checkout</h4>
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>SL No.</th>	
							<th>Product</th>
							<th>Quality</th>
							<th>Product Name</th>
							<th>Price</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
							<?php
								$varQuery = mysqli_query($connection, "SELECT * FROM cart LEFT JOIN product ON product.product_code = cart.product_code WHERE customer_id = '".$_SESSION['CustomerId']."'");
								$varCount = 0;
								$varTotal = 0;
								while ($varData = mysqli_fetch_array($varQuery)) {
									$varCount = $varCount+1;
									$varCode = $varData['product_code'];
									$varName = $varData['product_name'];
									$varQuantity = $varData['count'];
									$varPrice = $varData['product_price_sale'];
								$varSqlImg = mysqli_query($connection, "SELECT * FROM image WHERE product_code='$varCode' ORDER BY image_id DESC LIMIT 1");
								$varDataImg = mysqli_fetch_array($varSqlImg);
								$varImage = $varDataImg['image_name'];
							?>							
						<tr class="rem1">
						<td class="invert"><?php echo $varCount; ?></td>
						<td class="invert-image"><a href="single.html"><img src="../Admin/img/<?php echo $varImage; ?>" alt=" " class="img-responsive"></a></td>
						<td class="invert">
							 <div class="quantity"> 
								<div class="quantity-select">                           
									<div class="entry value-minus">&nbsp;</div>
									<div class="entry value"><span><?php echo $varQuantity; ?></span></div>
									<div class="entry value-plus active">&nbsp;</div>
								</div>
							</div>
						</td>
						<td class="invert"><?php echo $varName; ?></td>
						
						<td class="invert">Rp. <?php echo number_format($varPrice); ?></td>
						<td class="invert"><?php echo "Rp.".number_format($Subtotal=$varQuantity*$varPrice); ?></td>
						
					</tr>
				<?php 
				$varTotal = $varTotal+($varQuantity*$varPrice);
			} ?>
				<tr>
					<td colspan="4">Total</td>
					<td>
						<?php echo number_format($varTotal); ?>
					</td>
					<td></td>
				</tr>
				</tbody></table>
			</div>
			
									</section>
								</form>
						<br>			
				<div class="col-md-8 address_form_agile">
					  <h4>Add a new Details</h4>

				<form action="cartProccess.php" method="post" class="creditly-card-form agileinfo_form">
						<input type="hidden" name="total" value="<?php echo $varTotal; ?>">
							<?php
								$varQuery = mysqli_query($connection, "SELECT * FROM customer WHERE customer_id = '".$_SESSION['CustomerId']."'");
								$varData = mysqli_fetch_array($varQuery);
									$varCustomerName = $varData['customer_name'];
									$varPhoneNumber	= $varData['customer_phone'];
									$varCustomerAddress = $varData['customer_address'];
							?>						
									<section class="creditly-wrapper wthree, w3_agileits_wrapper">
										<div class="information-wrapper">
											<div class="first-row form-group">
												<div class="controls">
													<label class="control-label">Full name: </label>
													<input class="billing-address-name form-control" name="name" type="text" name="name" placeholder="Full name" value="<?php echo $varCustomerName ?>">
												</div>
												<div class="w3_agileits_card_number_grids">
													<div class="w3_agileits_card_number_grid_left">
														<div class="controls">
															<label class="control-label">Mobile number:</label>
														    <input class="form-control" name="phone" type="text" placeholder="Mobile number" value="<?php echo $varPhoneNumber ?>">
														</div>
													</div>
													<div class="w3_agileits_card_number_grid_right">
														<div class="controls">
															<label class="control-label">Address: </label>
														 <input class="form-control" name="city" type="text" placeholder="Address" value="<?php echo $varCustomerAddress ?>">
														</div>
													</div>
													<div class="clear"> </div>
												</div>
											</div>
											<button name="checkout" class="submit check_out">Delivery to this Address</button>
										</div>
									</section>
								</form>
									<div class="checkout-right-basket">
				        	<a href="payment.php">Make a Payment <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
			      	</div>
					</div>
			
					</div>
				<div class="clearfix"> </div>
				
			</div>

		</div>
<!-- //about -->
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->

 <?php include "inc_footer.php"; ?>