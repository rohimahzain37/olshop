<?php 

include_once '../inc_connection.php';
	session_start();

	if (!isset($_SESSION['CustomerId'])) {
		header("location:login.php");
	} else {

	}

 ?>
  <?php include "inc_header.php"; ?>
<!-- about -->
		<div class="privacy about">
			<h3>Chec<span>kout</span></h3>
			
	      <div class="checkout-right">
				<h4>Your shopping cart contains: <span></span></h4>
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>No.</th>	
							<th>ID Booking</th>
							<th>Total Pembayaran</th>
							<th>Remove</th>
						</tr>
					</thead>
					<tbody>
								<?php
								$varQuery = mysqli_query($connection, "SELECT * FROM detail_booking WHERE customer_id = '".$_SESSION['CustomerId']."'");
								$varCount = 0;
								$varTotal = 0;
								while ($varData = mysqli_fetch_array($varQuery)) {
									$varCount = $varCount+1;
									$varIDBooking = $varData['id_detail_booking'];
									$varTotalPembelian = $varData['total_pembelian'];
							?>							
						<tr class="rem1">
						<td class="invert"><?php echo $varCount; ?></td>
						<td class="invert"><?php echo $varIDBooking; ?></td>
						<td class="invert"><?php echo $varTotalPembelian; ?></td>
						<td>
							<a href="detail_transaction.php?id=<?php echo $varIDBooking; ?>">Detail</a>

						</td>
					</tr>
				<?php } ?>
													
						
				</tbody></table>
			</div>

	      <div class="checkout-right">
				<h4>Your shopping cart contains: <span></span></h4>
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>No.</th>	
							<th>ID Transaction</th>
							<th>Total Pembayaran</th>
							<th>Nota</th>
						</tr>
					</thead>
					<tbody>
								<?php
								$varQuery = mysqli_query($connection, "SELECT * FROM detail_transaction WHERE customer_id = '".$_SESSION['CustomerId']."'");
								$varCount = 0;
								$varTotal = 0;
								while ($varData = mysqli_fetch_array($varQuery)) {
									$varCount = $varCount+1;
									$varIDBooking = $varData['id_detail_booking'];
									$varIDTransaction = $varData['id_detail_transaction'];
									$varTotalPembelian = $varData['total_pembelian'];
							?>							
						<tr class="rem1">
						<td class="invert"><?php echo $varCount; ?></td>
						<td class="invert"><?php echo $varIDTransaction; ?></td>
						<td class="invert"><?php echo $varTotalPembelian; ?></td>
						<td>
							<a target="_blank" href="nota.php?id=<?php echo $varIDBooking; ?>">Nota</a>

						</td>
					</tr>
				<?php } ?>
													
						
				</tbody></table>
			</div>
			
									</section>
								</form>
									
					</div>
					
				<div class="clearfix"> </div>
				
			</div>

		</div>
<!-- //about -->
		</div>
		<div class="clearfix"></div>
	</div>
<!-- //banner -->



<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-3 w3_footer_grid">
				<h3>information</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="events.php">Events</a></li>
					<li><a href="about.php">About Us</a></li>
					<li><a href="product.php">Product</a></li>
					<li><a href="services.php">Services</a></li>
					<li><a href="short-codes.php">Short Codes</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>policy info</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="faqs.php">FAQ</a></li>
					<li><a href="privacy.php">privacy policy</a></li>
					<li><a href="privacy.php">terms of use</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>what in stores</h3>
				<ul class="w3_footer_grid_list">
					<li><a href="pet.php">Pet Food</a></li>
					<li><a href="frozen.php">Frozen Snacks</a></li>
					<li><a href="kitchen.php">Kitchen</a></li>
					<li><a href="product.php">Branded Foods</a></li>
					<li><a href="household.php">Households</a></li>
				</ul>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>twitter posts</h3>
				<ul class="w3_footer_grid_list1">
					<li><label class="fa fa-twitter" aria-hidden="true"></label><i>01 day ago</i><span>Non numquam <a href="#">http://sd.ds/13jklf#</a>
						eius modi tempora incidunt ut labore et
						<a href="#">http://sd.ds/1389kjklf#</a>quo nulla.</span></li>
					<li><label class="fa fa-twitter" aria-hidden="true"></label><i>02 day ago</i><span>Con numquam <a href="#">http://fd.uf/56hfg#</a>
						eius modi tempora incidunt ut labore et
						<a href="#">http://fd.uf/56hfg#</a>quo nulla.</span></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
			<div class="agile_footer_grids">
				<div class="col-md-3 w3_footer_grid agile_footer_grids_w3_footer">
					<div class="w3_footer_grid_bottom">
						<h4>100% secure payments</h4>
						<img src="images/card.png" alt=" " class="img-responsive" />
					</div>
				</div>
				<div class="col-md-3 w3_footer_grid agile_footer_grids_w3_footer">
					<div class="w3_footer_grid_bottom">
						<h5>connect with us</h5>
						<ul class="agileits_social_icons">
							<li><a href="#" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#" class="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#" class="google"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#" class="dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="wthree_footer_copy">
				<p>© 2016 Grocery Store. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
			</div>
		</div>
	</div> 
	
	<?php include "inc_footer.php"; ?>