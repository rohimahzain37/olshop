 <?php include "inc_header.php"; ?>

<!-- login -->
		<div class="w3_login">
			<h3>Sign In & Sign Up</h3>
			<div class="w3_login_module">
				<div class="module form-module">
				  <div class="toggle"><i class="fa fa-times fa-pencil"></i>
					<div class="tooltip">Click Me</div>
				  </div>
				  <div class="form">
					<h2>Login to your account</h2>
					<form action="loginCheck.php" method="post">
					  <input type="email" name="email" placeholder="Email" required="true">
					  <input type="password" name="pass" placeholder="Password" required="true">
					  <input type="submit" value="Login">
					</form>
				  </div>
				  <div class="form">
					<h2>Create an account</h2>
					<form action="registerSave.php" method="post" enctype="multipart/form-data">
					  <input type="text" name="name" placeholder="Name" required="true">
					  <input type="text" name="address" placeholder="Address" required="true">
					  <input type="text" name="phone" placeholder="Phone Number" required="true">
					  <input type="email" name="email" placeholder="Email" required="true">
					  <input type="password" name="password" placeholder="Password" required="true">
					  <input type="text" name="identity" placeholder="Identity Number" required="true">
					  <input type="radio" name="gender" value="M"> Male
					  <input type="radio" name="gender" value="F"> Female <br>
					  <br>
					  
					  <input type="submit" value="Register" name="register">
					</form>
				  </div>
				  <div class="cta"><a href="#">Forgot your password?</a></div>
				</div>
			</div>
			<script>
				$('.toggle').click(function(){
				  // Switches the Icon
				  $(this).children('i').toggleClass('fa-pencil');
				  // Switches the forms  
				  $('.form').animate({
					height: "toggle",
					'padding-top': 'toggle',
					'padding-bottom': 'toggle',
					opacity: "toggle"
				  }, "slow");
				});
			</script>
		</div>
<!-- //login -->
		</div>
		<div class="clearfix"></div>
	</div>
	 <?php include "inc_footer.php"; ?>